import { useState, useEffect } from 'react';
import Post from './components/Post';
import Pagination from './components/Pagination/Pagination';
import usePagination from './hooks/usePagination';

const App = () => {
	const [posts, setPosts] = useState([]);
	const {
		changePage,
		setPage,
		firstElementIndex,
		lastElementIndex,
		currentPage,
		paginationNumbers
	} = usePagination({
		totalElements: posts.length,
		elementsPerPage: 10
	})

	useEffect(() => {
		fetch('https://jsonplaceholder.typicode.com/posts')
			.then((response) => response.json())
			.then((json) => setPosts(json));
	}, []);

	return (
		<div>
			<h1>Pagination Test</h1>
			<p>Page {currentPage}</p>
			<Pagination
				changePage={changePage}
				setPage={setPage}
				paginationNumbers={paginationNumbers}
			/>
			{posts
				.slice(firstElementIndex, lastElementIndex)
				.map(({id, title, body}) => (
				<Post
					key={id}
					id={id}
					title={title}
					body={body}
				/>
			))}
		</div>
	);
}

export default App;
