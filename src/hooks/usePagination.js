import { useState } from 'react';

const usePagination = ({ totalElements, elementsPerPage}) => {
	const [currentPage, setCurrentPage] = useState(1);
	const totalPages = Math.ceil(totalElements / elementsPerPage);
	const maxPaginationNumbers = totalPages >= 4 ? 4 : totalPages;
	const lastElementIndex = currentPage * elementsPerPage;
	const firstElementIndex = lastElementIndex - elementsPerPage;

	const getFirstPaginationNumber = (currentPage, totalPages) => {
		let result;
		if(currentPage === 1 || totalPages <= maxPaginationNumbers) {
			result = 1;
		}
		else if (currentPage < totalPages - maxPaginationNumbers + 1) {
			result = currentPage;
		}
		else if (currentPage <= totalPages) {
			result = totalPages - maxPaginationNumbers + 1;
		}
		return result;
	};

	const getLastPaginationNumber = (firstPaginationNumber, totalPages) => {
		const minNumberPages = Math.min(totalPages, maxPaginationNumbers);
		return firstPaginationNumber + minNumberPages - 1;
	};

	const getPaginationNumbers = (firstPaginationNumber, lastPaginationNumber) => {
		return Array.from({
			length: lastPaginationNumber - firstPaginationNumber +1
		}, (_, index) => firstPaginationNumber + index);
	};

	const firstPaginationNumber = getFirstPaginationNumber(currentPage, totalPages);
	const lastPaginationNumber = getLastPaginationNumber(firstPaginationNumber, totalPages);
	const paginationNumbers = getPaginationNumbers(firstPaginationNumber, lastPaginationNumber);


	const handleNextOrPrevious = (direction) => {
		if((currentPage === 1 && direction === "left") || (currentPage === totalPages && direction === "right")) return;
		const newActivePage = direction === "right" ? currentPage + 1 : currentPage - 1;
		setCurrentPage(newActivePage);
	};

	return {
		changePage: (direction) => handleNextOrPrevious(direction),
		setPage: (page) => setCurrentPage(page),
		firstElementIndex,
		lastElementIndex,
		currentPage,
		paginationNumbers
	}
};

export default usePagination;
