import React from 'react'

const Post = ({ id, title, body }) => {
    return (
        <div>
            <h1>ID {id}</h1>
            <h2>{title}</h2>
            <p>{body}</p>
        </div>
    )
};

export default Post;
