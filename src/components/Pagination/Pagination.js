const Pagination = props => {
	const {
		changePage,
		setPage,
		paginationNumbers
	} = props;

	return (
		<div>
			<button onClick={() => changePage("left")}>PREV</button>
			{paginationNumbers.map((pageNumber) => (
				<button
					onClick={() => setPage(pageNumber)}
					key={pageNumber}
				>
					{pageNumber	}
				</button>
			))}
			<button onClick={() => changePage("right")}>NEXT</button>
		</div>
	);
};

export default Pagination;